// //panggil core module http
// let http = require("http");
// //panggil core module filesystem
const fs = require("fs");
//panggil core module path
const path = require("path");
//panggil module express
const express = require("express");
const axios = require("axios");
//assign module express ke var expr
const expr = express();
const serveStat = require("serve-static");
//assign file json berisikan data random dari json generator
let rawData = fs.readFileSync("datas.json");
//assign data dalam bentuk array object
const datas = require("./datas.js");
//assign data yang telah diparsing data JSON yang diterima
const parsing = JSON.parse(rawData);
//destructuring object menjadi var dari sortData.js
const { data1, data2, data3, data4, data5 } = require("./sortData.js");
const { assert } = require("console");
//const { default: axioss } = require("axios");
//assign data parsing data JSON yang hendak dikirim
const data = JSON.stringify(datas);
//panggil modul dotenv
require("dotenv").config();
//destructuring object menjadi var dari .env
const { PORT = 8080 } = process.env;

//deklarasi function untuk render file html
function getHTML(htmlFileName) {
  const htmlFilePath = path.join(dir, htmlFileName);
  return fs.readFileSync(htmlFilePath, "utf-8");
}

//assign directory path untuk get html
const dir = path.join(__dirname, "resources/views");
expr.use(serveStat(dir));
const nodemodules = path.join(__dirname, "node_modules");
expr.use(serveStat(nodemodules));
const img = path.join(__dirname, "resources/img");
expr.use(serveStat(img));
const script = __dirname;
expr.use(serveStat(script));

//assign directory path untuk get css
const aset = path.join(__dirname, "resources/css/");

// set the view engine to ejs
expr.set('view engine', 'ejs');
expr.set('views', path.join(__dirname, '/resources/views'));
expr.get("/", (req, res) => {
  res.render("homepage");
});
expr.get("/data1", (req, res) => {
  let datas = data1(parsing);
  res.render("data1", { datas, });
});
expr.get("/data2", (req, res) => {
  let datas = data2(parsing);
  res.render("data2", { datas, });
});
expr.get("/data3", (req, res) => {
  let datas = data3(parsing);
  res.render("data3", { datas, });
});
expr.get("/data4", (req, res) => {
  let datas = data4(parsing);
  res.render("data4", { datas, });
});
expr.get("/data5", (req, res) => {
  let datas = data5(parsing);
  res.render("data5", { datas, });
});
expr.get("/about", (req, res) => {
  res.render("about");
});
expr.use(function (req, res, next) {
  res.status(404).send(getHTML("404.html"));
});

//assign port untuk listen
expr.listen(PORT, function () {
  console.log("Example app listening on port 3000.");
});