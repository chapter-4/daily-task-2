module.exports = {
  // sort data based on age < 30 and fruit banana
  data1: function (datas) {
    let bulk = [];
    datas.forEach((key, value) => {
      key.age < 30 && key.favoriteFruit === "banana" ? bulk.push(key) : bulk.push()
    })
    return bulk.length > 1 ? bulk : JSON.stringify({ message: "Sorry, data gk ada" })
  },
  // sort data based on gender == female or company FSW4 and age > 30
  data2: function (datas) {
    let bulk = [];
    datas.forEach((key, value) => {
      key.gender === "female" || (key.company === "FSW4" && key.age > 30) ? bulk.push(key) : bulk.push()
    })
    return bulk.length > 1 ? bulk : JSON.stringify({ message: "Sorry, data gk ada" })
  },
  //sort data based on blue eyecolor and age >= 35 and age <= 40 and fruit Apple
  data3: function (datas) {
    let bulk = [];
    datas.forEach((key, value) => {
      key.eyeColor === "blue" && (key.age >= 35 && key.age <= 40) && key.favoriteFruit === "apple" ? bulk.push(key) : bulk.push()
    })
    return bulk.length > 1 ? bulk : JSON.stringify({ message: "Sorry, data gk ada" })
  },
  //sort data based on company and eyecolor green
  data4: function (datas) {
    let bulk = [];
    datas.forEach((key, value) => {
      (key.company === "Pelangi" || key.company === "Intel") && key.eyeColor === "green" ? bulk.push(key) : bulk.push()
    })
    return bulk.length > 1 ? bulk : JSON.stringify({ message: "Sorry, data gk ada" })
  },
  //sort data based on company and eyecolor green
  data5: function (datas) {
    let bulk = [];
    datas.forEach((key, value) => {
      //sort data based on registered year < 2016
      key.registered.split("-")[0] < 2016 && key.eyeColor === "green" ? bulk.push(key) : bulk.push()
    })
    return bulk.length > 1 ? bulk : JSON.stringify({ message: "Sorry, data gk ada" })
  }
}